//const signsManager = require('./signsManager');
const connectedUsers = require('./startup/userWs').users;
//const commands = require('./routes/commands');

function checkEvent(ws, event) {
    
    if(!ws.programList) return;

    // find item in program list
    for (let item of ws.programList) {
        if (item.id == event.data.id) {
            event.data = item;
            for (let user of connectedUsers) {
                user.emit('message', event);
            }
            break;
        }
    }
}

module.exports.checkEvent = checkEvent;