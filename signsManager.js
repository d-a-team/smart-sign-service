const log = require('./global/console');
const Sign = require('./models/sign');
var signs = [];

function newSign(ws) {
    signs.push(new Sign(ws));
}

function removeSign(ws) {
    for (let i = 0; i < signs.length; i++) {
        if (signs[i].ws == ws) {
            signs.splice(i, 1);
            return;
        }
    }
}

function setSignSerialNumber(ws, serialNumber) {
    for (let sign of signs) {
        if (sign.ws == ws) {
            sign.serialNumber = serialNumber;
            return;
        }
    }
}

module.exports.newSign = newSign;
module.exports.removeSign = removeSign;
module.exports.setSignSerialNumber = setSignSerialNumber;