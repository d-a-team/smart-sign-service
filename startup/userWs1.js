const express = require('express');
const app = express();
const settings = require('../settings.json');
const log = require('../global/console');
var httpProtocol = null;

var users = [];

/* ************** using HTTP ************** */
if (!settings.https) {
    httpProtocol = require('http').Server(app);
}
/********************************************/

/* ************** using HTTPS ************** */
else {
    const fs = require('fs');

    const options = {
        key: fs.readFileSync('key.pem'),
        cert: fs.readFileSync('crt.pem')
    }

    httpProtocol = require('https').Server(options, app);
}
/*********************************************/

const io = require('socket.io')(httpProtocol);

io.on('connection', (socket) => {
    // here you can pass parameters on connection with WS server
    let parameters = socket.handshake.query;

    // save socket in array of users
    users.push(socket);
    log.msg('new user connected. total is: ' + users.length);

    // send message example
    //socket.emit('message', `You are connected: ${socket.id}`);

    // listen to get message
    socket.on('message', (data) => {
        // messageHandler.checkMessageType(data, socket);
    })

    // listen to on disconnect
    socket.on('disconnect', () => {
        // search for connection and remove from array
        for (let i = 0; i < users.length; i++) {
            if (users[i].id == socket.id) {
                users.splice(i, 1);
                break;
            }
        }
        log.alert('user disconnected. total is: ' + users.length);
        /*  console.log(`Deleting socket: ${socket.id}`);
         messageHandler.sockets.delete(socket);
         console.log(`Remaining sockets: ${messageHandler.sockets.size}`); */
    });
});


httpProtocol.listen(4001);

var protocol = 'http' + (settings.https ? 's' : '');
var domain = settings.ip + ':' + settings.userWs;
log.normal(`User WS messages at ${protocol}://${domain}`);

module.exports.users = users;