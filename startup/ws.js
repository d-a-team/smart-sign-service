const WebSocket = require('ws');
const log = require('../global/console');
const signsManager = require('../signsManager');
const settings = require('../settings.json');
const commandResponse = require('../command-response');
const events = require('../events');
const userWs = require('./userWs');

var server = null;

/* ************** using HTTP ************** */
if (!settings.https) {
    server = require('http').createServer();
}
/********************************************/

/* ************** using HTTPS ************** */
else {
    // Minimal amount of secure websocket server
    var fs = require('fs');

    // read ssl certificate
    var privateKey = fs.readFileSync('key.pem', 'utf8');
    var certificate = fs.readFileSync('crt.pem', 'utf8');

    const constants = require('constants');

    var credentials = {
        key: privateKey,
        cert: certificate,
        secureOptions: constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_TLSv1_1 | constants.SSL_OP_NO_SSLv2 | constants.SSL_OP_NO_SSLv3
    };
    var https = require('https');


    //pass in your credentials to create an https server
    server = https.createServer(credentials);
}
/*********************************************/

server.listen(settings.ws);

//var WebSocketServer = require('ws').Server;
var wss = new WebSocket.Server({
    server: server
});

var protocol = 'ws' + (settings.https ? 's' : '');
var domain = settings.ip + ':' + settings.ws;
log.normal(`Device WS messages at ${protocol}://${domain}`);

var clients = [];

wss.on('connection', async function connection(ws) {

    // for use in the commands section
    ws.timeouts = {};

    clients.push(ws);
    log.msg('New device conneted. total: ' + clients.length);
    signsManager.newSign(ws);

    ws.on('message', function incoming(data) {
        let message = JSON.parse(data);
        if (message.type == 'command-response')
            commandResponse.checkCommandResponse(ws, message);
        else if (message.type == 'event')
            events.checkEvent(ws, message);

        log.normal('Received: ' + data);
    });

    ws.on('close', () => {
        signsManager.removeSign(ws);
        for (let i = 0; i < clients.length; i++) {
            if (clients[i] == ws) {
                clearTimeout(clients[i].timeouts.alarm);
                clearTimeout(clients[i].timeouts.light);
                clients.splice(i, 1);
                break;
            }
        }
        log.alert('Device connection closed. total: ' + clients.length);

        //clients.pop();
        ws.close();


        let reconnectionEvent = false;
        for (let client of clients) {
            if (client.serialNumber == ws.serialNumber) {
                reconnectionEvent = true;
                break;
            }
        }

        if (!reconnectionEvent) {
            let newStatusMsg = {
                type: 'event',
                serialNumber: ws.serialNumber,
                data: {
                    type: 'connection',
                    status: {
                        active: false
                    }
                }
            };
            userWs.sendEvent(ws.serialNumber, newStatusMsg);
        }
    });

    //get program list from sign
    let sendData = {
        type: "command",
        socketId: '*',
        data: {
            "code": "get-program-list"
        }
    }
    await sendCommand(null, sendData, ws).then(res => {
        ws.serialNumber = res.serialNum;
        ws.programList = res.data.data.paramarray;
    }).catch(() => {

    });

    //send get status when first connection
    sendData.data = {
        "code": "get-status"
    }
    await sendCommand(ws.serialNumber, sendData).then(res => {
        ws.status = res.data;

        let newStatusMsg = {
            type: 'event',
            serialNumber: ws.serialNumber,
            data: {
                type: 'connection',
                status: {
                    active: true,
                    wifi: ws.status.wifi.status,
                    wifiName: ws.status.wifi.wifiName
                }
            }
        };
        userWs.sendEvent(ws.serialNumber, newStatusMsg);
    });
});

sendCommand = function (serialNumber, data, ws) {
    return new Promise((resolve, reject) => {

        if (!clients.length) {
            reject(new Error('no connected clients'));
            return;
        }

        if (serialNumber !== null)
            for (let client of clients) {
                if (client.serialNumber == serialNumber) {
                    client.send(JSON.stringify(data));
                }
            }
        else
            ws.send(JSON.stringify(data));

        let timeout = new Date();
        timeout.setSeconds(timeout.getSeconds() + 30);

        // wait for command-response from device
        let localInterval = setInterval(() => {

            // if device did not response for more then 30 seconds throw error to user
            if (timeout.getTime() < new Date().getTime()) {
                reject(new Error('Time out. device did not response in 30 seconds...'));
                return;
            }

            for (let i = 0; i < commandResponse.stackOfResponses.length; i++) {
                // if same id and same command
                if (commandResponse.stackOfResponses[i].socketId == data.socketId &&
                    commandResponse.stackOfResponses[i].commandCode == data.data.code) {
                    // stop searching
                    clearInterval(localInterval);
                    // send item as answer
                    resolve(JSON.parse(JSON.stringify(commandResponse.stackOfResponses[i])));
                    // delete response from list
                    commandResponse.stackOfResponses.splice(i, 1);

                    break;
                }
            }
        }, 200);
    });
}

function getClient(serialNumber) {
    for (let client of clients) {
        if (client.serialNumber == serialNumber) {
            return client;
        }
    }

    throw (new Error('Client with serialNumber ' + serialNumber + ' not connected'));
}

module.exports.sendCommand = sendCommand;
module.exports.clients = clients;
module.exports.getClient = getClient;