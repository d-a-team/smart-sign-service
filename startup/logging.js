// logger - for loggin our errors
const winston = require('winston');

// for handaling erros in the route handler
// we don't need to write a middleware function to handler the errors
// example for an error: the database stoped working
require('express-async-errors');

module.exports = function () {


    // handle uncaught exception
    // if some error accured during the startup this function will handle that error and log it 
    // the proccess will not terminate
    // works only with synchronized code
    winston.handleExceptions(

        // show error on the console
        new winston.transports.Console({
            colorize: true,
            prettyPrint: true
        }),
        // write error to the file
        new winston.transports.File({
            filename: 'uncaughtExceptions.log'
        }));

    // handle unhandled rejection
    // works with asynchronized code 
    // for example call to a database or call to a remote http service
    process.on('unhandledRejection', (ex) => {
        console.log("WE GOT ERROR REJECTION");
        console.log(ex.message);
        winston.error(ex.message, ex);
        //throw ex;
    });

    // loggin to file
    winston.add(winston.transports.File, {
        filename: 'logfile.log',
        level: 'info'
    });

}