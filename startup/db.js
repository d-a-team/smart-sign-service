const settings = require('../settings.json');
const mysql = require('mysql');
const util = require('util');

var pool = mysql.createPool({
    connectionLimit: 5,
    host: settings.db.host,
    user: settings.db.user,
    password: settings.db.password,
    multipleStatements: true
});

var activeConneciton = false;

function query(q) {
    return new Promise((resolve, reject) => {

        pool.getConnection((err, connection) => {
            // error in conneciton
            if (err) {
                reject(err);

                activeConneciton = false;
                return;
            }

            if (!activeConneciton) {
                console.log('\x1b[32m\x1b[1m%s\x1b[0m', 'Database reconnected!');
                activeConneciton = true;
            }

            connection.query(q, (err, rows) => {
                // error in query
                if (err) {
                    reject(err);
                }
                else
                    resolve(rows);

                connection.release();
            });
        });
    });
}

module.exports.query = query;