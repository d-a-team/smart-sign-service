const log = require('../global/console');
const settings = require('../settings.json');

var users = [];

function handleSocket(io) {
    var protocol = 'http' + (settings.https ? 's' : '');
    var domain = settings.ip + ':' + settings.server;
    log.normal(`User WS messages at ${protocol}://${domain}`);

    io.on('connection', (socket) => {
        // here you can pass parameters on connection with WS server
        let parameters = socket.handshake.query;
        socket.listenToSN = parameters.serialNumber;

        // save socket in array of users
        users.push(socket);
        log.msg('new user connected. total is: ' + users.length);
        log.msg('new user Ip: ' + socket.handshake.address.split(":")[3]);

        // send message example
        //socket.emit('message', `You are connected: ${socket.id}`);

        // listen to get message
        socket.on('message', (data) => {
            // messageHandler.checkMessageType(data, socket);
        })

        // listen to on disconnect
        socket.on('disconnect', () => {
            // search for connection and remove from array
            for (let i = 0; i < users.length; i++) {
                if (users[i].id == socket.id) {
                    users.splice(i, 1);
                    break;
                }
            }
            log.alert('user disconnected. total is: ' + users.length);
            /*  console.log(`Deleting socket: ${socket.id}`);
             messageHandler.sockets.delete(socket);
             console.log(`Remaining sockets: ${messageHandler.sockets.size}`); */
        });
    });
}

function sendEvent(serialNumber, msg){
    for(let user of users){
        //if(user.listenToSN == serialNumber){
            user.emit('message', msg);
        //}
    }
}

module.exports.handleSocket = handleSocket;
module.exports.sendEvent = sendEvent;
module.exports.users = users;
