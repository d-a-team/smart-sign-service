class Sign {
    constructor(webSocket) {
        this.ws = webSocket;
        this.serialNumber = null;
        this.currentState = {
            lights: { state: false, color: null },
            alert: false,
            image: null,
            video: null,
            buildingNumber: null,
            text: { id: null, text: null, duration: null }
        }
        this.memoryData = {
            images: [],
            videos: [],
            texts: []
        }
    }
}

module.exports = Sign;