var listOfCommands = {
    getStatus: {
        code: 'get-status',
        params: {}
    },

    setProgramList: {
        code: 'set-program-list',
        params: {}
    },

    getProgramList: {
        code: 'get-program-list',
        params: {}
    },

    activateLight: {
        code: 'activate-light',
        params: {
            color: null,
            duration: null,
            blinkState: false,
            blinkTime: null, // milisecounds
            type: ''
        }
    },

    deactivateLight: {
        code: 'deactivate-light',
        params: {}
    },

    activateAlarm: {
        code: 'activate-alarm',
        params: {
            duration: null
        }
    },

    deactivateAlarm: {
        code: 'deactivate-alarm',
        params: {}
    },

    updateBuildingNumber: {
        code: 'set-building-number',
        params: {
            buildingNumber: null,
            subText: null
        }
    },

    openDoor: {
        code: 'open-door',
        params: {
            phoneNumber: null
        }
    },

    openGate: {
        code: 'open-gate',
        params: {
            phoneNumber: null
        }
    },

    openCamera: {
        code: 'open-camera',
        params: {}
    },

    closeCamera: {
        code: 'close-camera',
        params: {}
    },

    getWifi: {
        code: 'get-wifi-connection',
        params: {}
    },

    connectWifi: {
        code: 'connect-to-wifi',
        params: {
            name: null,
            password: null,
            wifi: null
        }
    },

    disconnectWifi: {
        code: 'disconnect-to-wifi',
        params: {
            name: null,
            password: null,
            wifi: null
        }
    },

    sendSms: {
        code: 'send-sms',
        params: {
            number: "+8618651215510",
            msg: "Hi sulin como setas"
        }
    },


    restartSign: {
        code: 'restart-sign',
        params: {}
    }
}

module.exports = listOfCommands;