class SignEvent {
    constructor() {
        this.type = '';
        this.data = {
            code: ''
        };
    }
}

class Command {
    constructor(commandData, socketId) {
        this.type = 'command';
        this.socketId = socketId;
        this.data = {
            code: commandData.code,
            params: commandData.params
        };;
    }
}

module.exports.SignEvent = SignEvent;
module.exports.Command = Command;