const signsManager = require('./signsManager');
//const commands = require('./routes/commands');

var stackOfResponses = [];

function checkCommandResponse(ws, message) {
    stackOfResponses.push(message);
    switch (message.commandCode) {
        case 'get-status': { // get the current status of sign
            signsManager.setSignSerialNumber(ws, message.serialNumber);
            // commands.programList = message.programList;
            // send event of current state to users
            break;
        }
        case 'get-program-list': { // get the program list of sign
            
            if (!ws.programList)
            ws.programList = [];
            
            ws.programList.length = 0;
            
            if(!message.data.data.paramarray) return;
            for (let item of message.data.data.paramarray)
                ws.programList.push(item);

            break;
        }
    }
}

module.exports.checkCommandResponse = checkCommandResponse;
module.exports.stackOfResponses = stackOfResponses;