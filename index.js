const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const log = require('./global/console');
//require('./startup/userWs');

var settings = require('./settings.json');

var httpProtocol = null;

/* ************** using HTTP ************** */
if (!settings.https) {
  httpProtocol = require('http').Server(app);
}
/********************************************/

/* ************** using HTTPS ************** */
else {
  const fs = require('fs');

  const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('crt.pem')
  }

  httpProtocol = require('https').Server(options, app);
}
/*********************************************/

// start the handle the socket
io = require('socket.io')(httpProtocol);
require('./startup/userWs').handleSocket(io);

// allow access from other services
app.use(cors());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.append('Access-Control-Allow-Headers', 'Content-Type');
  
  next();
})

// support parsing of application/json type post data
app.use(bodyParser.json({
  limit: "50mb"
}));

// support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({
  limit: "50mb",
  extended: true
}));

// decrease the size of the response body and hence increase the speed of a web app
app.use(compression());

// HTTP request logger
app.use(morgan('tiny'));

require('./startup/logging')();

// set the restful API routes
require('./startup/routes')(app);

// connect to database
require('./startup/db');

httpProtocol.listen(settings.server, () => {
  let protocol = 'http' + (settings.https ? 's' : '');
  let domain = settings.ip + ':' + settings.server;
  log.normal('API messages at ' + protocol + '://' + domain);
});

function setTerminalTitle(title) {
  process.stdout.write(
    String.fromCharCode(27) + "]0;" + title + String.fromCharCode(7)
  );
}

setTerminalTitle('Smart sign service');