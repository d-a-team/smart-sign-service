const express = require('express');
const router = express.Router();
const ws = require('../startup/ws');
const listOfCommands = require('../models/listOfCommands');
const classes = require('../models/classes');
const log = require('../global/console');
const userWs = require('../startup/userWs');

/*********************************/
/********** API : Get Status **********/
/*********************************/
/* 
    Get the current status of the sign.
    - the current text in memory.
    - the current images in memory.
    - the current videos in memory. 
    - the current state of lights.
    - the current building number.
    - the current alarm state.
    
    # Paramenters #
    serialNumber: string,
    command: string,

    # Response #
    Object: {
        serialNum: string,
        textData:  Array<{ id: number, text: string, duration: number }>,
        imageData: Array<{ id: number, image: string, duration: number }>,
        videoData: Array<{ id: number, video: string }>,
        buildingNumber:  number,
        lightState: {state:boolean, color:string},
        alarmState: {state:boolean}
    }
*/
router.post('/get-status', async (req, res) => {
    try {
        log.info('API: get-status, from: ' + req._remoteAddress);
        let serialNumber = req.body.serialNumber;

        let client = ws.getClient(serialNumber);

        for (let item of client.programList) {
            if (item.id == client.status.currentView.id) {
                client.status.currentView = item;
                break;
            }
        }

        res.send(client.status);

    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/




/******************************************/
/********** API : activate light **********/
/******************************************/
/* 
    # Description #
    - activate light in smart-sign.
    - the code of command is changed by 'color' parameter that passed threw API request
    
    # Paramenters #
    serialNumber: string,
    color: string,
    duration: number

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/activate-light', async (req, res) => {
    try {
        log.info('API: activate-light');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateLight
        let command = new classes.Command(listOfCommands.activateLight, req.body.socketId);

        // set color
        command.data.params.color = req.body.light.color;
        // set duration
        command.data.params.duration = req.body.light.duration;
        // set blinkState
        command.data.params.blinkState = req.body.light.blinkState;
        command.data.params.blinkTime = req.body.light.blinkTime;
        command.data.params.type = req.body.light.type;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        if (wsResult.status) {
            let client = ws.getClient(serialNumber);

            client.status.lightState.color = req.body.light.color;
            client.status.lightState.duration = req.body.light.duration;
            client.status.lightState.blinkState = req.body.light.blinkState;
            client.status.lightState.blinkTime = +req.body.light.blinkTime;
            client.status.lightState.type = req.body.light.type;
            client.status.lightState.state = true;

            let newStatusMsg = {
                type: 'event',
                serialNumber: serialNumber,
                data: {
                    type: 'lightState',
                    status: client.status.lightState
                }
            };
            userWs.sendEvent(serialNumber, newStatusMsg);

            // deactivate lights after duration is over
            client.timeouts.light = setTimeout(() => {
                client.status.lightState.state = false;

                let newStatusMsg = {
                    type: 'event',
                    serialNumber: serialNumber,
                    data: {
                        type: 'lightState',
                        status: client.status.lightState
                    }
                };
                userWs.sendEvent(serialNumber, newStatusMsg);
            }, client.status.lightState.duration * 60000);
        }

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/******************************************/
/********** API : deactivate light **********/
/******************************************/
/* 
    # Description #
    - deactivate light in smart-sign.
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/deactivate-light', async (req, res) => {
    try {
        log.info('API: deactivate-light');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateLight
        let command = new classes.Command(listOfCommands.deactivateLight, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        if (wsResult.status) {
            let client = ws.getClient(serialNumber);
            client.status.lightState.state = false;

            let newStatusMsg = {
                type: 'event',
                serialNumber: serialNumber,
                data: {
                    type: 'lightState',
                    status: client.status.lightState
                }
            };
            userWs.sendEvent(serialNumber, newStatusMsg);

            if (client.timeouts.light) {
                clearTimeout(client.timeouts.light);
                client.timeouts.light = null;
            }
        }

        //send the response to the front
        res.send(wsResult);

    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/******************************************/
/********** API : activate alarm **********/
/******************************************/
/* 
    # Description #
    - activate alarm sound.
    
    # Paramenters #
    serialNumber: string,
    duration: number

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/activate-alarm', async (req, res) => {
    try {
        log.info('API: activate-alarm');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateAlarm
        let command = new classes.Command(listOfCommands.activateAlarm, req.body.socketId);
        // set duration
        command.data.params.duration = req.body.alarm.duration;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        if (wsResult.status) {
            let client = ws.getClient(serialNumber);
            client.status.alarmState.state = true;
            client.status.alarmState.duration = req.body.alarm.duration;

            let newStatusMsg = {
                type: 'event',
                serialNumber: serialNumber,
                data: {
                    type: 'alarmState',
                    status: client.status.alarmState
                }
            };
            userWs.sendEvent(serialNumber, newStatusMsg);

            // deactivate alarm after duration is over
            client.timeouts.alarm = setTimeout(() => {
                client.status.alarmState.state = false;

                let newStatusMsg = {
                    type: 'event',
                    serialNumber: serialNumber,
                    data: {
                        type: 'alarmState',
                        status: client.status.alarmState
                    }
                };
                userWs.sendEvent(serialNumber, newStatusMsg);
            }, client.status.alarmState.duration * 60000);
        }

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/******************************************/
/********** API : de-activate alarm **********/
/******************************************/
/* 
    # Description #
    - deactivate alarm sound.
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/deactivate-alarm', async (req, res) => {
    try {
        log.info('API: deactivate-alarm');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateAlarm
        let command = new classes.Command(listOfCommands.deactivateAlarm, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        if (wsResult.status) {
            let client = ws.getClient(serialNumber);
            client.status.alarmState.state = false;

            let newStatusMsg = {
                type: 'event',
                serialNumber: serialNumber,
                data: {
                    type: 'alarmState',
                    status: client.status.alarmState
                }
            };
            userWs.sendEvent(serialNumber, newStatusMsg);

            if (client.timeouts.alarm) {
                clearTimeout(client.timeouts.alarm);
                client.timeouts.alarm = null;
            }
        }

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/*********************************************/
/********** API : set sign's program list **********/
/*********************************************/
/* 
    # Description #
    - set program list of sign
    
    # Paramenters #
    serialNumber: string,
    programList: any[];

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/

router.post('/set-program-list', async (req, res) => {
    try {
        log.info('API: set-program-list');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateAlarm
        let command = new classes.Command(listOfCommands.setProgramList, req.body.socketId);

        command.data.params = {};
        command.data.paramarray = req.body.programList;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //if return success update the program list
        if (wsResult.status) {
            let programList = ws.getClient(serialNumber).programList;
            if (programList)
                programList.length = 0;
            else
                programList = [];

            for (let item of command.data.paramarray)
                programList.push(item);
        }

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

/*****************************************************/


/*********************************************/
/********** API : get sign's program list **********/
/*********************************************/
/* 
    # Description #
    - get program list of sign
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/

router.post('/get-program-list', async (req, res) => {
    try {
        log.info('API: get-program-list');
        let serialNumber = req.body.serialNumber;

        //send the response to the front
        res.send(ws.getClient(serialNumber).programList);

    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

/*****************************************************/



/*********************************************/
/********** API : set building number **********/
/*********************************************/
/* 
    # Description #
    - set building number and sub text
    
    # Paramenters #
    serialNumber: string,
    buildingNumber: {number: number, subtext: string};

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/

router.post('/set-building-number', async (req, res) => {
    try {
        log.info('API: set-building-number');
        let serialNumber = req.body.serialNumber;

        // create new command of type activateAlarm
        let command = new classes.Command(listOfCommands.updateBuildingNumber, req.body.socketId);

        command.data.params.buildingNumber = req.body.buildingNumber.number;
        command.data.params.subText = req.body.buildingNumber.subText;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        if (wsResult.status) {
            let client = ws.getClient(serialNumber);
            client.status.buildingNumber.number = req.body.buildingNumber.number;
            client.status.buildingNumber.subText = req.body.buildingNumber.subText;

            let newStatusMsg = {
                type: 'event',
                serialNumber: serialNumber,
                data: {
                    type: 'buildingNumber',
                    status: client.status.buildingNumber
                }
            };
            userWs.sendEvent(serialNumber, newStatusMsg);
        }

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});

/*****************************************************/



/*************************************/
/********** API : open door **********/
/*************************************/
/* 
    Open door using phonenumber
    
    # Paramenters #
    serialNumber: string,
    phoneNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/open-door', async (req, res) => {
    try {
        log.info('API: open-door');
        let serialNumber = req.body.serialNumber;

        // create new command of type openDoor
        let command = new classes.Command(listOfCommands.openDoor, req.body.socketId);
        // set phoneNumber
        command.data.params.phoneNumber = req.body.phoneNumber;

        //test - delete
        command.data.params.phoneNumber = "+8618651215510";
        //test - delete

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/




/*************************************/
/********** API : open gate **********/
/*************************************/
/* 
    Open gate using phone number
    
    # Paramenters #
    serialNumber: string,
    phoneNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/open-gate', async (req, res) => {
    try {
        log.info('API: open-gate');
        let serialNumber = req.body.serialNumber;

        // create new command of type openDoor
        let command = new classes.Command(listOfCommands.openGate, req.body.socketId);

        // set phoneNumber
        command.data.params.phoneNumber = req.body.phoneNumber;

        //test - delete
        command.data.params.phoneNumber = "+8618651215510";
        //test - delete

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/*************************************/
/********** API : open camera **********/
/*************************************/
/* 
    Open camera
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/open-camera', async (req, res) => {
    try {
        log.info('API: open-camera');
        let serialNumber = req.body.serialNumber;

        // create new command of type openCamera
        let command = new classes.Command(listOfCommands.openCamera, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/*************************************/
/********** API : close camera **********/
/*************************************/
/* 
    Close camera
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/close-camera', async (req, res) => {
    try {
        log.info('API: close-camera');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.closeCamera, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/



/********************************************/
/********** API : get wifi connections **********/
/********************************************/
/* 
    get list of wifi detected by the sign
    
    # Paramenters #
    serialNumber: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/get-wifi', async (req, res) => {
    try {
        log.info('API: get-wifi');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.getWifi, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/



/********************************************/
/********** API : connect to wifi **********/
/********************************************/
/* 
    Connect to wifi
    
    # Paramenters #
    serialNumber: string,
    wifi: string,
    name: string,
    password: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/connect-wifi', async (req, res) => {
    try {
        log.info('API: connect-wifi');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.connectWifi, req.body.socketId);

        // set wifi
        command.data.params.wifi = req.body.connectionData.wifi.wifiName;
        // set name
        //command.data.params.name = req.body.connectionData.wifi.userName;
        // set password
        command.data.params.password = req.body.connectionData.wifi.password;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/





/**********************************************/
/********** API : disconnect to wifi **********/
/**********************************************/
/* 
    disconnect to wifi
    
    # Paramenters #
    serialNumber: string,
    wifi: string,
    name: string,
    password: string

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/disconnect-wifi', async (req, res) => {
    try {
        log.info('API: disconnect-wifi');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.disconnectWifi, req.body.socketId);

        // set wifi
        command.data.params.wifi = req.body.connectionData.wifi.wifiName;
        // set name
        command.data.params.name = req.body.connectionData.wifi.userName;
        // set password
        command.data.params.password = req.body.connectionData.wifi.password;

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/


/********************************************/
/********** API : send-sms **********/
/********************************************/
/* 
    send message in sms 
    
    # Paramenters #
    serialNumber: string
    socketId : string 
    smsData:{
        phoneNumber:string,
        messageText:string
    }

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/send-sms', async (req, res) => {
    try {
        log.info('API: send-sms');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.sendSms, req.body.socketId);
        command.data.params.number = req.body.smaData.phoneNumber;
        command.data.params.msg = req.body.smaData.messageText;

        //test
        command.data.params.number = "+8618651215510";
        command.data.params.msg = "Hi sulin como setas";
        //test

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/





/********************************************/
/********** API : restart-sign **********/
/********************************************/
/* 
    send message in sms 
    
    # Paramenters #
    serialNumber: string
    socketId : string 

    # Response #
    Object: {
        command: string,
        commandStatus: string
    }
*/
router.post('/restart-sign', async (req, res) => {
    try {
        log.info('API: restart-sign');
        let serialNumber = req.body.serialNumber;

        // create new command of type closeCamera
        let command = new classes.Command(listOfCommands.restartSign, req.body.socketId);

        //send to smart sign the action in WS
        log.msg('sent to sign:');
        log.msg(JSON.stringify(command));
        let wsResult = await ws.sendCommand(serialNumber, command);

        //send the response to the front
        res.send(wsResult);
    } catch (ex) {
        log.error(ex.message);
        res.status(500).send(ex.message);
    }
});
/*****************************************************/



// right the action in logs
logCommand = function (command, sender) {
    /* var query = `
            SELECT * FROM dbo.buildings as b WHERE b.SerialNumber = '${serialNumber}';
        `;

        var response = await db.query(query);

        var resObj = {
            buildingIndex: response[0].BuildingIndex,
            serialNumber: response[0].SerialNumber,
            address: response[0].Address,
            numOfFloors: response[0].NumOfFloors,
            numOfApartments: response[0].NumOfApartments,
            elevator: response[0].Elevator
        }

        res.send(resObj); */
}

module.exports.router = router;
//module.exports.programList = programList;